**I would consider this project obsolete as a much better option
exists now** see stash

https://github.com/appscode/stash


A tool for performing restic backups of rook pvcs. All commands are non destructive aside from restoring a pvc from backup. I use it for my backups and restores. I have used it for about 5 Tb of backups and 30 pvcs.

# Commands
## Initialize
Example: `python rook-backup.py init <namespace> <pvc> --backup-directory=<backupdir>`

Initializes a backup image for namespace/pvc in backup directory. The restic repositories are organized as follows <backupdir>/<namespace>/<pvc>. Restic will always ask for passwords unless you set `RESTIC_PASSWORD`.

## Backup
Example: `python rook-backup.py backup <namespace> <pvc> --backup-directory=<backupdir>`

backup namespace/pvc. Most importantly it performs a `fsfreeze --freeze` before snapshotting the rbd then performs an `fsfreeze --unfreeze` afterwards. **Requires** ssh root access to each node. It will ask for password/passphrase if not supplied.

## Restore
Example: `python rook-backup.py restore <namespace> <pvc> --backup-directory=<backupdir>`

**WARNING** this is a destructive command. If you pass the `--clean` option it will additionally reformat the rbd to a ext4 filesystem. Performs restic restore on the pvc. This means that restic will overwrite all matching files.

## Info
Example: `python rook-backup.py info [<namespace>] [<pvc>]`

Lists distilled information about rook pvc setup. Information includes which pods have pvc mounted, whether pvc is bound to a pod, if backup exists for namespace, etc.

## Snapshots
Example: `python rook-backup.py snapshots <namespace> <pvc> --backup-directory=<backupdir>`

List all restic snapshots for given namespace/pvc

## Mount
Example: `python rook-backup.py mount <namespace> <pvc>`

Mounts given namespace/pvc to `/mnt/<namespace>/<pvc>`. Ctrl-C to automatically unmount pvc.


# Dependencies:
 - Available commands on node: `restic`, `kubectl`, and `ceph` (specifically rdb command)
 - Only uses python stdlib so it should run on python2.7+, python3.3+
 - must be run on a node in the kubernetes cluster.

# Assumptions:
 - Rook saves configuration information at `/var/lib/rook`
 - You are performing backups only on pvcs with a StorageClass.
 - Ceph maps devices to `/dev/rbd/<poolname>/<image>`
 - When ceph rbds are mounted they are mounted at `/mnt/<namespace>/<pvc>`
 - When filesystems are frozen with `fsfreeze` the filesystems are expected at `/var/lib/kubelet/plugins/rook.io/rook/mounts/<image>` on each node.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details
