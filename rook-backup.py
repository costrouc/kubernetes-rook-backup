#!/usr/env python
from __future__ import print_function

import subprocess
import os
import contextlib
import json
import logging
import collections
import argparse
import time
import random
import string


class RookBackup:
    def __init__(self, backup_directory, ceph_args=None):
        self.backup_directory = backup_directory
        self.ceph_args = ceph_args or ['--cluster=rook', '--conf=/var/lib/rook/rook/rook.config', '--keyring=/var/lib/rook/rook/client.admin.keyring']
        self.logger = logging.getLogger(self.__class__.__name__)

    def print_pvcs_backup_info(self, pvcs):
        data_format = '{:<16}{:<32} {:<64} {:>8}'
        header = data_format.format('NAMESPACE', 'PVC', 'BACKUP_DIR', 'SIZE')
        print(header)
        for namespace in pvcs:
            for pvc in pvcs[namespace]:
                restic_backup_directory = os.path.join(self.backup_directory, namespace, pvc)
                if not os.path.isdir(restic_backup_directory):
                    restic_backup_directory = None
                size = None
                if restic_backup_directory:
                    size = subprocess.check_output(['du', '-sh', os.path.join(restic_backup_directory, 'data')]).split()[0]
                print(data_format.format(namespace, pvc, restic_backup_directory, size))
        print()

    def print_pvcs_ceph_mapping(self, pvcs):
        data_format = '{:<16}{:<32}{:<24}{:<16}{:<64}{:<8}'
        header = data_format.format('NAMESPACE', 'PVC', 'STORAGECLASS', 'POOL', 'IMAGE', 'MAPPED')
        print(header)
        for namespace in pvcs:
            for pvc in pvcs[namespace]:
                pvc_info = self.pvc_info(namespace, pvc)
                rbd_image = pvc_info['image']
                storageclass = pvc_info['storageclass']
                rbd_pool = pvc_info['pool']
                rbd_mapped = 'true' if pvc_info['mapped'] else 'false'
                print(data_format.format(namespace, pvc, storageclass, rbd_pool, rbd_image, rbd_mapped))
        print()

    def print_pvcs_pod_mapping(self, pvcs):
        data_format = '{:<16}{:<32} {:<16}{:<64}{:<16}{:<32}'
        header = data_format.format('NAMESPACE', 'PVC', 'NODE', 'POD', 'CONTAINER', 'MOUNT_PATH')
        print(header)
        pvc_mounts = self.pod_pvc_info(pvcs)
        for key, values in pvc_mounts.items():
            if key[1] not in pvcs[key[0]]:
                continue
            if len(values):
                print(data_format.format(key[0], key[1], values[0][0], values[0][1], values[0][2], values[0][3]))
                for value in values[1:]:
                    print(data_format.format('', '', value[0], value[1], value[2], value[3]))
        print()

    def pvc_info(self, namespace, pvc):
        response = subprocess.check_output(['kubectl', 'get', '-n', namespace, 'pvc', pvc, '-o', 'json'])
        data = json.loads(response)
        pvc_data = {'image': data['spec']['volumeName'], 'pool': None, 'storageclass': None, 'mapped': None}
        if data['metadata']['annotations'].get('volume.beta.kubernetes.io/storage-class', data['spec'].get('storageClassName')):
            pvc_data['storageclass'] = data['metadata']['annotations'].get('volume.beta.kubernetes.io/storage-class', data['spec'].get('storageClassName'))
            storageclass_info = self.storageclass_info(pvc_data['storageclass'])
            pvc_data['pool'] = storageclass_info.get('pool')

        if pvc_data.get('pool') and pvc_data.get('image'):
            pvc_data['mapped'] = os.path.exists('/dev/rbd/%s/%s' % (pvc_data['pool'], pvc_data['image']))
        return pvc_data

    def storageclass_info(self, storageclass):
        response = subprocess.check_output(['kubectl', 'get', 'storageclass', storageclass, '-o', 'json'])
        data = json.loads(response)
        return {'pool': data['parameters']['pool']}

    def pod_pvc_info(self, pvcs):
        pvc_mounts = collections.defaultdict(list)
        for namespace in pvcs:
            response = subprocess.check_output(['kubectl', 'get', 'pods', '-n', namespace, '-o', 'json'])
            data = json.loads(response)
            for pod in data['items']:
                pod_name = pod['metadata']['name']
                node_name = pod['spec']['nodeName']
                claim_names = {}
                for claim in pod['spec']['volumes']:
                    if 'persistentVolumeClaim' in claim:
                        claim_names[claim['name']] = claim['persistentVolumeClaim']['claimName']
                for container in pod['spec']['containers']:
                    container_name = container['name']
                    for volume in container['volumeMounts']:
                        if volume['name'] in claim_names:
                            claim_name = claim_names[volume['name']]
                            pvc_mounts[(namespace, claim_name)].append([node_name, pod_name, container_name, volume['mountPath']])
        return pvc_mounts

    def check_pvc_exists(self, namespace, pvc):
        rook_pvcs = self.rook_pvcs()
        if namespace not in rook_pvcs or pvc not in rook_pvcs[namespace] is None:
            self.logger.critical('namespace or pvc does not exists')
            print('Available PVCS:')
            data_format = '{:<32}{:<32}'
            print(data_format.format('NAMESPACE', 'PVC'))
            for namespace in rook_pvcs:
                for pvc in rook_pvcs[namespace]:
                    print(data_format.format(namespace, pvc))
            exit(1)

    @contextlib.contextmanager
    def rbd_fsfreeze(self, nodes, rbd_images):
        try:
            frozen_filesystems = []
            for node, rbd_image in zip(nodes, rbd_images):
                filesystem_directory = '/var/lib/kubelet/plugins/rook.io/rook/mounts/%s' % rbd_image
                subprocess.call(['ssh', node, 'fsfreeze', '--freeze', filesystem_directory])
                self.logger.info('freeze %s %s' % (node, filesystem_directory))
                frozen_filesystems.append([node, filesystem_directory])
            yield
        finally:
            for node, filesystem_directory in frozen_filesystems:
                subprocess.call(['ssh', 'root@%s' % node, 'fsfreeze', '--unfreeze', filesystem_directory])
                self.logger.info('unfreeze %s %s' % (node, filesystem_directory))

    def create_rbd_snapshot(self, pool, image, snapshot="backup"):
        snapshot_image = '%s/%s@%s' % (pool, image, snapshot)
        subprocess.call(['rbd', 'snap', 'create', snapshot_image] + self.ceph_args)
        self.logger.info('rbd snapshot created %s' % snapshot_image)

    def delete_rbd_snapshot(self, pool, image, snapshot="backup"):
        snapshot_image = '%s/%s@%s' % (pool, image, snapshot)
        subprocess.call(['rbd', 'snap', 'remove', snapshot_image] + self.ceph_args)
        self.logger.info('rbd snapshot deleted %s' % snapshot_image)

    @contextlib.contextmanager
    def rbd_map(self, pool, image, snapshot=None):
        try:
            if snapshot:
                snapshot_image = '%s/%s@%s' % (pool, image, snapshot)
            else:
                snapshot_image = '%s/%s' % (pool, image)
            subprocess.call(['rbd', 'map', snapshot_image] + self.ceph_args)
            self.logger.info('rbd mapping image %s' % snapshot_image)
            yield
        finally:
            subprocess.call(['rbd', 'unmap', '/dev/rbd/%s' % snapshot_image] + self.ceph_args)
            self.logger.info('rbd unmapping image %s' % snapshot_image)

    @contextlib.contextmanager
    def mount(self, mount_device, mount_target, options=None):
        try:
            mount_command = ['mount']
            if options:
                mount_command += ['-o', options]
            mount_command += [mount_device, mount_target]
            retcode = subprocess.call(mount_command)
            if retcode != 0:
                self.logger.critical('mount failed %s %s' % (mount_device, mount_target))
                raise OSError('mount failed')
            else:
                self.logger.info('mount %s %s' % (mount_device, mount_target))
            yield
        finally:
            subprocess.call(['umount', mount_target])
            self.logger.info('unmount %s' % mount_target)

    def rook_pvcs(self):
        response = subprocess.check_output(['kubectl', 'get', 'pvc', '--all-namespaces', '-o', 'json'])
        data = json.loads(response)
        pvcs = collections.defaultdict(set)
        for pvc in data['items']:
            if pvc['metadata']['annotations'].get('volume.beta.kubernetes.io/storage-provisioner') == 'rook.io/block':
                pvcs[pvc['metadata']['namespace']].add(pvc['metadata']['name'])
        return pvcs

    def restic_snapshots(self, namespace, pvc):
        self.check_pvc_exists(namespace, pvc)
        print('\n%s' % namespace, pvc)
        subprocess.call(['restic', '-r', '%s/%s/%s' % (self.backup_directory, namespace, pvc), 'snapshots'])
        self.logger.info('restic snapshots  %s %s' % (namespace, pvc))

    def restic_init(self, namespace, pvc):
        self.check_pvc_exists(namespace, pvc)
        subprocess.call(['restic', '-r', '%s/%s/%s' % (self.backup_directory, namespace, pvc), 'init'])
        self.logger.info('restic init  %s %s' % (namespace, pvc))

    def backup_pvc(self, namespace, pvc):
        self.check_pvc_exists(namespace, pvc)
        pvc_info = self.pvc_info(namespace, pvc)
        rbd_pool = pvc_info['pool']
        rbd_image = pvc_info['image']
        snapshot_name = 'backup-' + ''.join([random.choice(string.ascii_lowercase) for _ in range(4)])
        rbd_device = '/dev/rbd/%s/%s@%s' % (rbd_pool, rbd_image, snapshot_name)
        rbd_mount_path = '/mnt/%s/%s' % (namespace, pvc)
        restic_backup_directory = os.path.join(self.backup_directory, namespace, pvc)
        if not os.path.exists(rbd_mount_path):
            os.makedirs(rbd_mount_path)

        pvc_mounts = self.pod_pvc_info({namespace: set(pvc)})
        pvc_hosts = [_[0] for _ in pvc_mounts[(namespace, pvc)]]
        pvc_images = ['%s' % rbd_image for _ in range(len(pvc_hosts))]
        self.logger.info('backup image: %s hosts: %s' % (rbd_image+'@'+snapshot_name, pvc_hosts))

        try:
            with self.rbd_fsfreeze(pvc_hosts, pvc_images):
                self.create_rbd_snapshot(rbd_pool, rbd_image, snapshot_name)
            with self.rbd_map(rbd_pool, rbd_image, snapshot_name):
                with self.mount(rbd_device, rbd_mount_path):
                    restic_command = ['restic', '-r', restic_backup_directory, 'backup', rbd_mount_path]
                    subprocess.call(' '.join(restic_command), shell=True)
        finally:
            self.delete_rbd_snapshot(rbd_image, snapshot_name)

    def mount_pvc(self, namespace, pvc):
        self.check_pvc_exists(namespace, pvc)
        pvc_info = self.pvc_info(namespace, pvc)
        if pvc_info.get('mapped'):
            self.logger.critical('rbd is currently mapped cannot modify in this state')
            exit(1)

        rbd_pool = pvc_info['pool']
        rbd_image = pvc_info['image']
        rbd_device = '/dev/rbd/%s/%s' % (rbd_pool, rbd_image)
        rbd_mount_path = '/mnt/%s/%s' % (namespace, pvc)
        restic_backup_directory = os.path.join(self.backup_directory, namespace, pvc)
        if not os.path.exists(rbd_mount_path):
            os.makedirs(rbd_mount_path)

        with self.rbd_map(rbd_pool, rbd_image):
            with self.mount(rbd_device, rbd_mount_path):
                print('RBD READY mounted at path: ', rbd_mount_path)
                try:
                    while True: # Fake look
                        time.sleep(100)
                except KeyboardInterrupt:
                    pass

    def restore_pvc(self, namespace, pvc, clean=False, version='latest'):
        self.check_pvc_exists(namespace, pvc)
        pvc_info = self.pvc_info(namespace, pvc)
        if pvc_info.get('mapped'):
            self.logger.critical('rbd is currently mapped cannot modify in this state')
            exit(1)

        rbd_pool = pvc_info['pool']
        rbd_image = pvc_info['image']
        rbd_device = '/dev/rbd/%s/%s' % (rbd_pool, rbd_image)
        rbd_mount_path = '/mnt/%s/%s' % (namespace, pvc)
        restic_backup_directory = os.path.join(self.backup_directory, namespace, pvc)
        if not os.path.exists(rbd_mount_path):
            os.makedirs(rbd_mount_path)

        with self.rbd_map(rbd_pool, rbd_image):
            if clean:
                retcode = subprocess.call(['mkfs.ext4', '-m0', rbd_device])
                if retcode != 0:
                    self.logger.critical('mkfs.ext4 failed')
                    raise ValueError('mkfs.ext4 failed')
            with self.mount(rbd_device, rbd_mount_path):
                restic_command = ['restic', '-r', restic_backup_directory, 'restore', '-t', '/mnt/%s' % namespace, version]
                subprocess.call(restic_command)


def backup_info(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rook_pvcs = rookbackup.rook_pvcs()

    # filter pvcs by namespace and name
    pvcs = collections.defaultdict(set)
    for namespace in rook_pvcs:
        if args.namespace and args.namespace != namespace:
            continue
        for pvc in rook_pvcs[namespace]:
            if args.pvc and args.pvc != pvc:
                continue
            pvcs[namespace].add(pvc)

    print('{:-^128}'.format('| RESTIC BACKUP |'))
    rookbackup.print_pvcs_backup_info(pvcs)
    print('{:-^128}'.format('| PERSISTENT VOLUME CLAIM LOCATIONS |'))
    rookbackup.print_pvcs_pod_mapping(pvcs)
    print('{:-^128}'.format('| CEPH PERSISTENT VOLUME CLAIM MAPPING |'))
    rookbackup.print_pvcs_ceph_mapping(pvcs)


def backup_init(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rookbackup.restic_init(args.namespace, args.pvc)


def backup_snapshots(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rookbackup.restic_snapshots(args.namespace, args.pvc)


def backup_mount(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rookbackup.mount_pvc(args.namespace, args.pvc)


def backup_backup(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rookbackup.backup_pvc(args.namespace, args.pvc)


def backup_restore(args):
    rookbackup = RookBackup(backup_directory=args.backup_directory)
    rookbackup.restore_pvc(args.namespace, args.pvc, clean=args.clean)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    DEFAULT_BACKUP_DIRECTORY = '/backup/kubernetes'

    def backup_parser_info(parser):
        parser.set_defaults(func=backup_info)
        parser.add_argument('-n', '--namespace')
        parser.add_argument('--pvc')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    def backup_parser_init(parser):
        parser.set_defaults(func=backup_init)
        parser.add_argument('namespace')
        parser.add_argument('pvc')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    def backup_parser_snapshots(parser):
        parser.set_defaults(func=backup_snapshots)
        parser.add_argument('namespace')
        parser.add_argument('pvc')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    def backup_parser_mount(parser):
        parser.set_defaults(func=backup_mount)
        parser.add_argument('namespace')
        parser.add_argument('pvc')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    def backup_parser_backup(parser):
        parser.set_defaults(func=backup_backup)
        parser.add_argument('namespace')
        parser.add_argument('pvc')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    def backup_parser_restore(parser):
        parser.set_defaults(func=backup_restore, clean=False)
        parser.add_argument('namespace')
        parser.add_argument('pvc')
        parser.add_argument('--clean', dest='clean', action='store_true')
        parser.add_argument('-b', '--backup-directory', default=DEFAULT_BACKUP_DIRECTORY)

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    backup_parser_info(subparsers.add_parser('info', help='print helpful information about pvcs'))
    backup_parser_init(subparsers.add_parser('init', help='initialize restic repository for namespace/pvc'))
    backup_parser_snapshots(subparsers.add_parser('snapshots', help='list restic snapshots for namespace/pvc'))
    backup_parser_mount(subparsers.add_parser('mount', help='mounts namespace/pvc'))
    backup_parser_backup(subparsers.add_parser('backup', help='backup namespace/pvc to restic repository'))
    backup_parser_restore(subparsers.add_parser('restore', help='restore namespace/pvc from restic repository'))
    args = parser.parse_args()
    args.func(args)
